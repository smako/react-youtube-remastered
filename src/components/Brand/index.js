import React, { Component } from "react";
import "./Brand.scss";
import { connect } from "react-redux";
import { actionTypes } from "../../store/rootReducer";
import { Link } from "react-router-dom";

class Brand extends Component {
  handleReset(e) {
    e.preventDefault();
    console.log("qqq");
    this.props.reset();
  }

  render() {
    return (
      <div className="brand">
        <Link to="/" onClick={this.handleReset.bind(this)}>
          <img
            src="http://pngimg.com/uploads/youtube/youtube_PNG13.png"
            className="brand__logo"
            alt=""
          />
        </Link>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {};
};

const mapDispatchToProps = (dispatch) => {
  return {
    reset: () => {
      dispatch({
        type: actionTypes.REQUEST_RESET,
      });
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Brand);
