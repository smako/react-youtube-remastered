import React from "react";
import VideoCard from "../VideoCard";
import "./style.scss";

function RecommendedVideos() {
  return (
    <div className="recommendedVideos">
      <h2>Recommendations</h2>
      <div className="recommendedVideos__videos">
        <VideoCard
          title="Créer son entreprise, plus qu'un projet, une nouvelle vie !"
          views="2.4M Vues"
          timestamp="Il y a 2 jours"
          channelImage="https://image.flaticon.com/icons/svg/1154/1154482.svg"
          channel="Emma Markson"
          image="https://image.freepik.com/free-photo/funny-happy-excited-young-pretty-woman-sitting-table-black-shirt-working-laptop-co-working-office-wearing-glasses_285396-86.jpg"
        />
        <VideoCard
          title="Le virus est toujours là, restez prudent !"
          views="6.1M Vues"
          timestamp="Il y a 5 jours"
          channelImage="https://image.flaticon.com/icons/svg/375/375228.svg"
          channel="Santé et bien-être"
          image="https://image.freepik.com/free-vector/prevent-epidemic-rebound-concept-illustration_114360-3008.jpg"
        />
        <VideoCard
          title="Faire du sport en musique, choisissez bien votre playlist !"
          views="850K Vues"
          timestamp="Il y a 1 semaine"
          channelImage="https://image.flaticon.com/icons/svg/2917/2917805.svg"
          channel="Sport Actif"
          image="https://image.freepik.com/free-photo/exited-sporty-woman-demonstrate-muscule-studio_273443-1245.jpg"
        />
        <VideoCard
          title="Bien manger n'est pas si sorcier !"
          views="560K Vues"
          timestamp="Il y a 2 jours"
          channelImage="https://image.flaticon.com/icons/svg/2983/2983413.svg"
          channel="Food for life"
          image="https://image.freepik.com/free-photo/delicious-pizza-wooden-table_23-2148643473.jpg"
        />
        <VideoCard
          title="Les micros rétro reviennent à la mode, qu'en pensez-vous ?"
          views="2.4M Vues"
          timestamp="Il y a 4 heures"
          channelImage="https://image.flaticon.com/icons/svg/1011/1011057.svg"
          channel="Retro box"
          image="https://image.freepik.com/free-photo/retro-microphone-notebook-computer-live-webcast-air-concept_1387-338.jpg"
        />
        <VideoCard
          title="Eu esse labore id nostrud aliqua sint irure ex."
          views=""
          timestamp=""
          channelImage=""
          channel=""
          image="https://www.angeliquesawtell.com/wp-content/themes/realestate-7/images/no-image.png"
        />
        <VideoCard
          title="Dolore occaecat non non esse nostrud culpa cupidatat."
          views=""
          timestamp=""
          channelImage=""
          channel=""
          image="https://www.angeliquesawtell.com/wp-content/themes/realestate-7/images/no-image.png"
        />
        <VideoCard
          title="Tempor consequat quis in pariatur sunt laborum."
          views=""
          timestamp=""
          channelImage=""
          channel=""
          image="https://www.angeliquesawtell.com/wp-content/themes/realestate-7/images/no-image.png"
        />
        <VideoCard
          title="Irure nisi eiusmod duis consequat ut aute nisi elit dolor cillum nulla occaecat eu consectetur."
          views=""
          timestamp=""
          channelImage=""
          channel=""
          image="https://www.angeliquesawtell.com/wp-content/themes/realestate-7/images/no-image.png"
        />
        <VideoCard
          title="Amet ullamco qui do veniam cupidatat nostrud duis sint tempor eiusmod."
          views=""
          timestamp=""
          channelImage=""
          channel=""
          image="https://www.angeliquesawtell.com/wp-content/themes/realestate-7/images/no-image.png"
        />
      </div>
    </div>
  );
}

export default RecommendedVideos;
