import React, { Component } from "react";
import "./SearchResults.scss";
import { connect } from "react-redux";
import VideoCard from "../VideoCard";

class SearchResults extends Component {
  render() {
    return (
      <div className="searchResults">
        <h2>Resultat pour "{this.props.term}" </h2>
        <div className="searchResults__videos">
          {this.props.data?.items?.map((item, key) => {
            let title = item.snippet.title;
            let views = "";
            let timestamp = "";
            let channelImage = "";
            let channel = item.snippet.channelTitle;
            let image = item.snippet.thumbnails.high.url;
            let videoURL = `https://youtube.com/watch?v=${item.id.videoId}`;

            return (
              <VideoCard
                key={key}
                title={title}
                views={views}
                timestamp={timestamp}
                channelImage={channelImage}
                channel={channel}
                image={image}
                videoURL={videoURL}
              />
            );
          })}
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  term: state.term,
  data: state.data,
  loading: state.loading,
});

export default connect(mapStateToProps)(SearchResults);
