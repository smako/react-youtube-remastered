import React, { Component } from "react";
import "./style.scss";
import { Avatar } from "@material-ui/core";
import PlayIcon from "@material-ui/icons/PlayArrow";

class VideoCard extends Component {
  render() {
    return (
      <a
        className="videoCard"
        href={this.props.videoURL || "#"}
        target="__blank"
      >
        <div className="videoCard__thumbnail">
          <div className="videoCard__thumbnail-overlay">
            <PlayIcon />
          </div>
          <img src={this.props.image} alt="" />
        </div>
        <div className="videoCard__info">
          <Avatar
            className="videoCard__avatar"
            alt={this.props.channel}
            src={this.props.channelImage}
          />
          <div className="videoCard__text">
            <h4>{this.props.title}</h4>
            <p>{this.props.channel}</p>
            <p>
              {this.props.views} - {this.props.timestamp}
            </p>
          </div>
        </div>
      </a>
    );
  }
}

export default VideoCard;
