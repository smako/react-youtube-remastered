import React, { Component } from "react";
import { connect } from "react-redux";
import "./SearchBar.scss";
import SearchIcon from "@material-ui/icons/Search";
import { actionTypes } from "../../store/rootReducer";
import youtubeAPI from "./../../hooks/youtube";

class SearchBar extends Component {
  state = {
    input: "",
  };

  setInput(value) {
    this.props.input(value);
    /* this.setState({
      input: value,
    }); */
  }

  async handleSearch(e) {
    e.preventDefault();

    if (this.props.term !== this.props.prec_term) {
      this.props.search();
      const response = await youtubeAPI.search(this.props.term);
      const data = response.data;
      this.props.searchSuccess(data);
    }
  }

  render() {
    return (
      <form className="searchBar" onSubmit={this.handleSearch.bind(this)}>
        <div className="searchBar__content">
          <div className="searchBar__field">
            <input
              type="text"
              autoComplete="off"
              required
              value={this.props.term}
              onChange={(e) => this.setInput(e.target.value)}
            />
            <label>
              <span>Recherche</span>
            </label>
          </div>
          <div className="searchBar__btn">
            <button type="submit">
              <SearchIcon />
            </button>
          </div>
        </div>
      </form>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    term: state.term,
    prec_term: state.prec_term,
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    input: (value) => {
      dispatch({
        type: actionTypes.SEARCH_INPUT,
        term: value,
      });
    },
    search: () => {
      dispatch({
        type: actionTypes.REQUEST_SEARCH,
      });
    },
    searchSuccess: (data) => {
      dispatch({
        type: actionTypes.REQUEST_SEARCH_SUCCESS,
        data: data,
      });
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(SearchBar);
