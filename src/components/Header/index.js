import React, { Component } from "react";
import { Link } from "react-router-dom";
import "./style.scss";

import HomeIcon from "@material-ui/icons/Home";
import WhatshotIcon from "@material-ui/icons/Whatshot";
import PlayArrowTwoToneIcon from "@material-ui/icons/PlayArrowTwoTone";
import BookIcon from "@material-ui/icons/Book";
import HistoryIcon from "@material-ui/icons/History";
import AccessTimeIcon from "@material-ui/icons/AccessTime";
import ThumbUpIcon from "@material-ui/icons/ThumbUp";
import GrainIcon from "@material-ui/icons/Grain";
import VideocamIcon from "@material-ui/icons/Videocam";
import NotificationsIcon from "@material-ui/icons/Notifications";
import { actionTypes } from "../../store/rootReducer";
import { connect } from "react-redux";

class Header extends Component {
  handleReset(e) {
    e.preventDefault();
    console.log("qqq");
    this.props.reset();
  }

  render() {
    return (
      <div className="header">
        {/* Logo - On the left -> img*/}
        <div className="header__brand">
          <Link to="/" onClick={this.handleReset.bind(this)}>
            <img
              className="header__brand-logo"
              src="https://www.freepngimg.com/thumb/youtube/24827-8-youtube-transparent.png"
              alt=""
            />
          </Link>
        </div>

        {/* MainNavbar */}
        <div className="header__menu">
          <nav className="header__nav">
            {/* MainNavbar */}
            <ul className="header__nav-main">
              <li>
                <Link to="/">
                  <HomeIcon />
                  <span>Accueil</span>
                </Link>
              </li>

              <li>
                <Link to="/trending">
                  <WhatshotIcon />
                  <span>Tendances</span>
                </Link>
              </li>

              <li>
                <Link to="/subscriptions">
                  <PlayArrowTwoToneIcon />
                  <span>Abonnements</span>
                </Link>
              </li>

              <li>
                <Link to="/library">
                  <BookIcon />
                  <span>Bibliothèque</span>
                </Link>
              </li>

              <li>
                <Link to="/history">
                  <HistoryIcon />
                  <span>Historique</span>
                </Link>
              </li>

              <li>
                <Link to="/later">
                  <AccessTimeIcon />
                  <span>À regarder plus tard</span>
                </Link>
              </li>

              <li>
                <Link to="/liked">
                  <ThumbUpIcon />
                  <span>Vidéos "J'aime"</span>
                </Link>
              </li>
            </ul>
            {/* SubNavbar */}
            <ul className="header__nav-sub">
              <li>
                <Link to="/create">
                  {/* Icon "Create" */}
                  <VideocamIcon />
                  <span className="subnav__list-item-text">Créer</span>
                </Link>
              </li>
              <li>
                <Link to="/apps">
                  {/* Icon "Apps" */}
                  <GrainIcon />
                  <span className="subnav__list-item-text">Applications</span>
                </Link>
              </li>
              <li>
                <Link to="/notifications">
                  {/* Icon "Notification" */}
                  <NotificationsIcon />
                  <span className="subnav__list-item-text">Notifications</span>
                </Link>
              </li>
              <li>
                <Link to="/account">
                  {/* Icon "Account" */}
                  <img
                    src={`https://avatars.dicebear.com/api/human/${Date.now()}.svg`}
                    alt=""
                  />
                  <span className="subnav__list-item-text">Votre compte</span>
                </Link>
              </li>
            </ul>
          </nav>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {};
};

const mapDispatchToProps = (dispatch) => {
  return {
    reset: () => {
      dispatch({
        type: actionTypes.REQUEST_RESET,
      });
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Header);
