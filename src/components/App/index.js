import React, { Component } from "react";
import "./App.scss";

import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

import HomePage from "../../pages/HomePage";
import Header from "../Header";

class App extends Component {
  render() {
    return (
      <div className="app">
        <Router>
          <Switch>
            <Route path="/search">{/* Search Page */}</Route>
            <Route path="/">
              {/* Header */}
              <Header />
              {/* Home */}
              <HomePage />
            </Route>
          </Switch>
        </Router>
      </div>
    );
  }
}

export default App;
