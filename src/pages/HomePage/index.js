import React, { Component } from "react";
import "./HomePage.scss";
import SearchBar from "../../components/SearchBar";
import { connect } from "react-redux";
import RecommendedVideos from "../../components/RecommendedVideos";
import SearchResults from "../../components/SearchResults";

class Home extends Component {
  render() {
    return (
      <div className="home">
        <div className="home__body">
          <div className="home__body-search">
            {/* SearchBar */}
            <SearchBar />
          </div>
          <div className="home__body-content">
            {this.props.data?.items?.length > 0 ? (
              <SearchResults />
            ) : (
              <RecommendedVideos />
            )}
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    term: state.term,
    data: state.data,
  };
};

export default connect(mapStateToProps)(Home);
