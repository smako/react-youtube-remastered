export const initialState = {
  term: "",
  prec_term: null,
  error: null,
  data: null,
  loading: false,
};

export const actionTypes = {
  SEARCH_INPUT: "SEARCH_INPUT",
  REQUEST_RESET: "REQUEST_RESET",
  REQUEST_SEARCH: "REQUEST_SEARCH",
  REQUEST_SEARCH_SUCCESS: "REQUEST_SEARCH_SUCCESS",
  REQUEST_SEARCH_ERROR: "REQUEST_SEARCH_ERROR",
};

const rootReducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.SEARCH_INPUT:
      return {
        ...state,
        term: action.term,
      };
    case actionTypes.REQUEST_RESET:
      return {
        ...initialState,
      };

    case actionTypes.REQUEST_SEARCH:
      return {
        ...state,
        loading: true,
      };

    case actionTypes.REQUEST_SEARCH_SUCCESS:
      return {
        ...state,
        prec_term: state.term,
        data: action.data,
        loading: false,
      };

    default:
      return state;
  }
};

export default rootReducer;
