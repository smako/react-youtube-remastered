import { API_KEY } from "./keys";
import Axios from "axios";
import data from "./data";

async function search(term) {
  try {
    let url = `https://www.googleapis.com/youtube/v3/search?part=snippet&order=viewCount&maxResults=20&type=video&key=${API_KEY}&q=${term}`;
    let response = await Axios.get(url);
    console.log(url);
    return response;
  } catch (error) {
    return data;
  }
}

/* function useYoutubeVideos() {
  console.log("useYoutubeVideos");
  console.log(data);
  return data;
} */

export default { search };
