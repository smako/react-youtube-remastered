const data = {
  kind: "youtube#searchListResponse",
  etag: "THCF5xLT-N6ZnBlUPVhm4878VCU",
  nextPageToken: "CBQQAA",
  regionCode: "FR",
  pageInfo: {
    totalResults: 1000000,
    resultsPerPage: 20,
  },
  items: [
    {
      kind: "youtube#searchResult",
      etag: "bfMgS-PoWyA5wn8A2QaquWQnVwA",
      id: {
        kind: "youtube#video",
        videoId: "BDx_YTf9x1g",
      },
      snippet: {
        publishedAt: "2018-06-26T12:00:03Z",
        channelId: "UCpzFPW_8KhW7M10biWXS5lg",
        title:
          "CAPO PLAZA - Tesla feat. Sfera Ebbasta, DrefGold (Prod. AVA, Charlie Charles, Daves The Kid)",
        description:
          'Ascolta il mio primo album "20" https://capoplaza.lnk.to/20 Capo Plaza “Tesla” feat. Sfera Ebbasta, DrefGold Prod. AVA, Charlie Charles, Daves The Kid Ascolta ...',
        thumbnails: {
          default: {
            url: "https://i.ytimg.com/vi/BDx_YTf9x1g/default.jpg",
            width: 120,
            height: 90,
          },
          medium: {
            url: "https://i.ytimg.com/vi/BDx_YTf9x1g/mqdefault.jpg",
            width: 320,
            height: 180,
          },
          high: {
            url: "https://i.ytimg.com/vi/BDx_YTf9x1g/hqdefault.jpg",
            width: 480,
            height: 360,
          },
        },
        channelTitle: "Capo Plaza",
        liveBroadcastContent: "none",
        publishTime: "2018-06-26T12:00:03Z",
      },
    },
    {
      kind: "youtube#searchResult",
      etag: "dTxStud9pG6vqAVj1_9GtIL8Xdk",
      id: {
        kind: "youtube#video",
        videoId: "l2q_-xN2N54",
      },
      snippet: {
        publishedAt: "2009-06-17T05:27:10Z",
        channelId: "UCEcLMPMI4b-pJpLrfnEvsvQ",
        title: "Tesla - Love Song",
        description:
          "REMASTERED IN HD! Music video by Tesla performing Love Song. (C) 1989 UMG Recordings, Inc. #Tesla #LoveSong #Remastered #Vevo #Rock ...",
        thumbnails: {
          default: {
            url: "https://i.ytimg.com/vi/l2q_-xN2N54/default.jpg",
            width: 120,
            height: 90,
          },
          medium: {
            url: "https://i.ytimg.com/vi/l2q_-xN2N54/mqdefault.jpg",
            width: 320,
            height: 180,
          },
          high: {
            url: "https://i.ytimg.com/vi/l2q_-xN2N54/hqdefault.jpg",
            width: 480,
            height: 360,
          },
        },
        channelTitle: "TeslaVEVO",
        liveBroadcastContent: "none",
        publishTime: "2009-06-17T05:27:10Z",
      },
    },
    {
      kind: "youtube#searchResult",
      etag: "m29w_FABH-YX8JS2oVkGl-IV_38",
      id: {
        kind: "youtube#video",
        videoId: "9vwHuCC6nP8",
      },
      snippet: {
        publishedAt: "2009-06-17T05:23:24Z",
        channelId: "UCEcLMPMI4b-pJpLrfnEvsvQ",
        title: "Tesla - What You Give",
        description:
          "REMASTERED IN HD! Music video by Tesla performing What You Give. (C) 1991 UMG Recordings, Inc. #Tesla #WhatYouGive #Remastered #Vevo #Rock ...",
        thumbnails: {
          default: {
            url: "https://i.ytimg.com/vi/9vwHuCC6nP8/default.jpg",
            width: 120,
            height: 90,
          },
          medium: {
            url: "https://i.ytimg.com/vi/9vwHuCC6nP8/mqdefault.jpg",
            width: 320,
            height: 180,
          },
          high: {
            url: "https://i.ytimg.com/vi/9vwHuCC6nP8/hqdefault.jpg",
            width: 480,
            height: 360,
          },
        },
        channelTitle: "TeslaVEVO",
        liveBroadcastContent: "none",
        publishTime: "2009-06-17T05:23:24Z",
      },
    },
    {
      kind: "youtube#searchResult",
      etag: "aGt3IYUZUe-pkjLnZT2yAf20nDk",
      id: {
        kind: "youtube#video",
        videoId: "m7atGkba-Z8",
      },
      snippet: {
        publishedAt: "2019-11-22T05:19:45Z",
        channelId: "UCddiUEpeqJcYeBxX1IVBKvQ",
        title: "Tesla Cybertruck event in 5 minutes",
        description:
          "Tesla CEO Elon Musk just unveiled the Cybertruck, the company's first electric pickup truck. There will be three versions of the truck — 250 miles, 300 miles, and ...",
        thumbnails: {
          default: {
            url: "https://i.ytimg.com/vi/m7atGkba-Z8/default.jpg",
            width: 120,
            height: 90,
          },
          medium: {
            url: "https://i.ytimg.com/vi/m7atGkba-Z8/mqdefault.jpg",
            width: 320,
            height: 180,
          },
          high: {
            url: "https://i.ytimg.com/vi/m7atGkba-Z8/hqdefault.jpg",
            width: 480,
            height: 360,
          },
        },
        channelTitle: "The Verge",
        liveBroadcastContent: "none",
        publishTime: "2019-11-22T05:19:45Z",
      },
    },
    {
      kind: "youtube#searchResult",
      etag: "gSLbH6XOL38IEsWU_sfOi9mgMQE",
      id: {
        kind: "youtube#video",
        videoId: "7m7FpzOxbkQ",
      },
      snippet: {
        publishedAt: "2009-12-25T06:39:07Z",
        channelId: "UCEcLMPMI4b-pJpLrfnEvsvQ",
        title: "Tesla - Paradise",
        description:
          "Best of Tesla: https://goo.gl/Na1FeM Subscribe here: https://goo.gl/Qe955k Music video by Tesla performing Paradise. (C) 1990 UMG Recordings, Inc. #Tesla ...",
        thumbnails: {
          default: {
            url: "https://i.ytimg.com/vi/7m7FpzOxbkQ/default.jpg",
            width: 120,
            height: 90,
          },
          medium: {
            url: "https://i.ytimg.com/vi/7m7FpzOxbkQ/mqdefault.jpg",
            width: 320,
            height: 180,
          },
          high: {
            url: "https://i.ytimg.com/vi/7m7FpzOxbkQ/hqdefault.jpg",
            width: 480,
            height: 360,
          },
        },
        channelTitle: "TeslaVEVO",
        liveBroadcastContent: "none",
        publishTime: "2009-12-25T06:39:07Z",
      },
    },
    {
      kind: "youtube#searchResult",
      etag: "hRTZdZoO-PdGEzrUxmM-A3MSb-M",
      id: {
        kind: "youtube#video",
        videoId: "qLvpLGFacjQ",
      },
      snippet: {
        publishedAt: "2019-03-09T09:00:12Z",
        channelId: "UCUhFaUpnq31m6TNX2VKVSVA",
        title:
          "Lamborghini Aventador S v Tesla Model S P100D - DRAG RACE, ROLLING RACE &amp; BRAKE TEST",
        description:
          "Coronavirus: Click here for advice for car buying during the pandemic - https://bit.ly/-Car-Buying-Advice This is it… The ultimate petrol vs electric showdown!",
        thumbnails: {
          default: {
            url: "https://i.ytimg.com/vi/qLvpLGFacjQ/default.jpg",
            width: 120,
            height: 90,
          },
          medium: {
            url: "https://i.ytimg.com/vi/qLvpLGFacjQ/mqdefault.jpg",
            width: 320,
            height: 180,
          },
          high: {
            url: "https://i.ytimg.com/vi/qLvpLGFacjQ/hqdefault.jpg",
            width: 480,
            height: 360,
          },
        },
        channelTitle: "carwow",
        liveBroadcastContent: "none",
        publishTime: "2019-03-09T09:00:12Z",
      },
    },
    {
      kind: "youtube#searchResult",
      etag: "nBNO2Bs_D1eDzzCEFR9nIdZcS-o",
      id: {
        kind: "youtube#video",
        videoId: "ckib1ABJ_sM",
      },
      snippet: {
        publishedAt: "2020-01-24T15:00:07Z",
        channelId: "UCnmGIkw-KdI0W5siakKPKog",
        title: "Tesla Autopilot For 24 Hours Straight!",
        description:
          "teslas really do be autopilotin doe Watch another video ▻ https://www.youtube.com/watch?v=1rwMH... My family channel ...",
        thumbnails: {
          default: {
            url: "https://i.ytimg.com/vi/ckib1ABJ_sM/default.jpg",
            width: 120,
            height: 90,
          },
          medium: {
            url: "https://i.ytimg.com/vi/ckib1ABJ_sM/mqdefault.jpg",
            width: 320,
            height: 180,
          },
          high: {
            url: "https://i.ytimg.com/vi/ckib1ABJ_sM/hqdefault.jpg",
            width: 480,
            height: 360,
          },
        },
        channelTitle: "Ryan Trahan",
        liveBroadcastContent: "none",
        publishTime: "2020-01-24T15:00:07Z",
      },
    },
    {
      kind: "youtube#searchResult",
      etag: "3qXerKxeCJLNOKNZ311zX35FkgM",
      id: {
        kind: "youtube#video",
        videoId: "tlThdr3O5Qo",
      },
      snippet: {
        publishedAt: "2019-04-23T01:47:07Z",
        channelId: "UC5WjFrtBdufl6CZojX3D8dQ",
        title: "Full Self-Driving",
        description: "https://ts.la/FSD.",
        thumbnails: {
          default: {
            url: "https://i.ytimg.com/vi/tlThdr3O5Qo/default.jpg",
            width: 120,
            height: 90,
          },
          medium: {
            url: "https://i.ytimg.com/vi/tlThdr3O5Qo/mqdefault.jpg",
            width: 320,
            height: 180,
          },
          high: {
            url: "https://i.ytimg.com/vi/tlThdr3O5Qo/hqdefault.jpg",
            width: 480,
            height: 360,
          },
        },
        channelTitle: "Tesla",
        liveBroadcastContent: "none",
        publishTime: "2019-04-23T01:47:07Z",
      },
    },
    {
      kind: "youtube#searchResult",
      etag: "-1jnsR7tui_eVX85_8Ziqsbhfy0",
      id: {
        kind: "youtube#video",
        videoId: "mr9kK0_7x08",
      },
      snippet: {
        publishedAt: "2018-08-20T21:08:09Z",
        channelId: "UCBJycsmduvYEL83R_U4JriQ",
        title: "Tesla Factory Tour with Elon Musk!",
        description:
          "Walking and talking through the Tesla factory Part 1 Interview: https://youtu.be/MevKTPN4ozw Behind-the-scenes with TLD: https://youtu.be/Tz9ZuI0PSSI ...",
        thumbnails: {
          default: {
            url: "https://i.ytimg.com/vi/mr9kK0_7x08/default.jpg",
            width: 120,
            height: 90,
          },
          medium: {
            url: "https://i.ytimg.com/vi/mr9kK0_7x08/mqdefault.jpg",
            width: 320,
            height: 180,
          },
          high: {
            url: "https://i.ytimg.com/vi/mr9kK0_7x08/hqdefault.jpg",
            width: 480,
            height: 360,
          },
        },
        channelTitle: "Marques Brownlee",
        liveBroadcastContent: "none",
        publishTime: "2018-08-20T21:08:09Z",
      },
    },
    {
      kind: "youtube#searchResult",
      etag: "ysqK6PgT2G-UT2NGgDqfSN-sQ8I",
      id: {
        kind: "youtube#video",
        videoId: "x2gh1NiLX18",
      },
      snippet: {
        publishedAt: "2018-11-05T11:00:39Z",
        channelId: "UC4PH8eYaYobiEYxnciJnuvQ",
        title: "GAZDA PAJA - TESLA (OFFICIAL VIDEO 2018)",
        description:
          "booking: +381 621645776 DAČA Slusaj preko svih streaming platformi: https://lnk.site/tesla Kupi pesmu preko sms-a : Srbija : 100 igroove medo na broj 1310 ...",
        thumbnails: {
          default: {
            url: "https://i.ytimg.com/vi/x2gh1NiLX18/default.jpg",
            width: 120,
            height: 90,
          },
          medium: {
            url: "https://i.ytimg.com/vi/x2gh1NiLX18/mqdefault.jpg",
            width: 320,
            height: 180,
          },
          high: {
            url: "https://i.ytimg.com/vi/x2gh1NiLX18/hqdefault.jpg",
            width: 480,
            height: 360,
          },
        },
        channelTitle: "Two Louder TV",
        liveBroadcastContent: "none",
        publishTime: "2018-11-05T11:00:39Z",
      },
    },
    {
      kind: "youtube#searchResult",
      etag: "okIqKJgUg4JCzDTXi0zv1JZpnlI",
      id: {
        kind: "youtube#video",
        videoId: "WG8y2KBH0xY",
      },
      snippet: {
        publishedAt: "2019-10-21T13:09:19Z",
        channelId: "UCeeHT-3WejdX6uGBgCbE7SA",
        title: "TESLA MODEL 3 460HP TOP SPEED DRIVE ON GERMAN AUTOBAHN 🏎",
        description:
          "TESLA MODEL 3 460HP Long Range AWD Dual Motor Top Speed Autobahn POV ( No Speed Limit ) #TopSpeedGermany Subscribe for more videos: ...",
        thumbnails: {
          default: {
            url: "https://i.ytimg.com/vi/WG8y2KBH0xY/default.jpg",
            width: 120,
            height: 90,
          },
          medium: {
            url: "https://i.ytimg.com/vi/WG8y2KBH0xY/mqdefault.jpg",
            width: 320,
            height: 180,
          },
          high: {
            url: "https://i.ytimg.com/vi/WG8y2KBH0xY/hqdefault.jpg",
            width: 480,
            height: 360,
          },
        },
        channelTitle: "TopSpeedGermany",
        liveBroadcastContent: "none",
        publishTime: "2019-10-21T13:09:19Z",
      },
    },
    {
      kind: "youtube#searchResult",
      etag: "DFSaxWRPctEgI315zM2pkgSRwSU",
      id: {
        kind: "youtube#video",
        videoId: "8_lfxPI5ObM",
      },
      snippet: {
        publishedAt: "2013-07-16T10:45:12Z",
        channelId: "UCftwRNsjfRo08xYE31tkiyw",
        title: "How the Tesla Model S is Made | Tesla Motors Part 1 (WIRED)",
        description:
          "If founder Elon Musk is right, Tesla Motors just might reinvent the American auto industry—with specialized robots building slick electric cars in a factory straight ...",
        thumbnails: {
          default: {
            url: "https://i.ytimg.com/vi/8_lfxPI5ObM/default.jpg",
            width: 120,
            height: 90,
          },
          medium: {
            url: "https://i.ytimg.com/vi/8_lfxPI5ObM/mqdefault.jpg",
            width: 320,
            height: 180,
          },
          high: {
            url: "https://i.ytimg.com/vi/8_lfxPI5ObM/hqdefault.jpg",
            width: 480,
            height: 360,
          },
        },
        channelTitle: "WIRED",
        liveBroadcastContent: "none",
        publishTime: "2013-07-16T10:45:12Z",
      },
    },
    {
      kind: "youtube#searchResult",
      etag: "qwQgFEW1vLpl_S545LpSAiLIAGg",
      id: {
        kind: "youtube#video",
        videoId: "gXGlmMMfkUA",
      },
      snippet: {
        publishedAt: "2020-02-27T23:31:30Z",
        channelId: "UCwD4x63A9KC7Si2RuSfg-SA",
        title: "Tesla Autopilot PRANK On Friends!",
        description:
          "Uhhh NOT GOOD. DOWNLOAD DOBRE DUNK! http://bit.ly/DownloadDOBREDUNK WANT A PERSONAL SHOUTOUT FROM US?! BOOK US ON CAMEO!",
        thumbnails: {
          default: {
            url: "https://i.ytimg.com/vi/gXGlmMMfkUA/default.jpg",
            width: 120,
            height: 90,
          },
          medium: {
            url: "https://i.ytimg.com/vi/gXGlmMMfkUA/mqdefault.jpg",
            width: 320,
            height: 180,
          },
          high: {
            url: "https://i.ytimg.com/vi/gXGlmMMfkUA/hqdefault.jpg",
            width: 480,
            height: 360,
          },
        },
        channelTitle: "Lucas and Marcus",
        liveBroadcastContent: "none",
        publishTime: "2020-02-27T23:31:30Z",
      },
    },
    {
      kind: "youtube#searchResult",
      etag: "4k8ryJji_PRawSws_ZzXFoYAMe0",
      id: {
        kind: "youtube#video",
        videoId: "qOIiYK90xTU",
      },
      snippet: {
        publishedAt: "2020-06-03T06:45:54Z",
        channelId: "UC6SPCnTAIanF2_8ST2wrQzw",
        title:
          "RAFFI AHMAD NGGAK MAU KALAH, LANGSUNG BELI 5 MOBIL TESLA TERBARU.. ANDRE KALAH TELAK",
        description:
          "FOLLOW INSTAGRAM TAULANY TV : https://www.instagram.com/taulany_tv/ taulany tv adalah channel yang sangat menghibur, serta ada berbagai karakter ...",
        thumbnails: {
          default: {
            url: "https://i.ytimg.com/vi/qOIiYK90xTU/default.jpg",
            width: 120,
            height: 90,
          },
          medium: {
            url: "https://i.ytimg.com/vi/qOIiYK90xTU/mqdefault.jpg",
            width: 320,
            height: 180,
          },
          high: {
            url: "https://i.ytimg.com/vi/qOIiYK90xTU/hqdefault.jpg",
            width: 480,
            height: 360,
          },
        },
        channelTitle: "TAULANY TV",
        liveBroadcastContent: "none",
        publishTime: "2020-06-03T06:45:54Z",
      },
    },
    {
      kind: "youtube#searchResult",
      etag: "lO5syeiPAsNSEx7ugjnthBzPN1k",
      id: {
        kind: "youtube#video",
        videoId: "3PZSGiWQMIk",
      },
      snippet: {
        publishedAt: "2019-11-26T17:48:00Z",
        channelId: "UC6HLPq1KVU8NIaFPeQIhJtg",
        title:
          "¿Es la Tesla Cybertruck una amenaza para la Ford F-150? | Univision Autos",
        description:
          "Es mejor camioneta que la F-150”, dijo Elon Musk sobre la Tesla Cybertruck. Una afirmación que no debería de tomarse a la ligera, sobre todo porque se ...",
        thumbnails: {
          default: {
            url: "https://i.ytimg.com/vi/3PZSGiWQMIk/default.jpg",
            width: 120,
            height: 90,
          },
          medium: {
            url: "https://i.ytimg.com/vi/3PZSGiWQMIk/mqdefault.jpg",
            width: 320,
            height: 180,
          },
          high: {
            url: "https://i.ytimg.com/vi/3PZSGiWQMIk/hqdefault.jpg",
            width: 480,
            height: 360,
          },
        },
        channelTitle: "UnivisionAutos",
        liveBroadcastContent: "none",
        publishTime: "2019-11-26T17:48:00Z",
      },
    },
    {
      kind: "youtube#searchResult",
      etag: "p9pbTFJTe1PXuYUc12NHd4474yQ",
      id: {
        kind: "youtube#video",
        videoId: "5RRmepp7i5g",
      },
      snippet: {
        publishedAt: "2017-12-14T18:17:31Z",
        channelId: "UC5WjFrtBdufl6CZojX3D8dQ",
        title: "Tesla Semi &amp; Roadster Unveil",
        description:
          "Semi is the safest, most comfortable truck ever. Four independent motors provide maximum power and acceleration and require the lowest energy cost per mile.",
        thumbnails: {
          default: {
            url: "https://i.ytimg.com/vi/5RRmepp7i5g/default.jpg",
            width: 120,
            height: 90,
          },
          medium: {
            url: "https://i.ytimg.com/vi/5RRmepp7i5g/mqdefault.jpg",
            width: 320,
            height: 180,
          },
          high: {
            url: "https://i.ytimg.com/vi/5RRmepp7i5g/hqdefault.jpg",
            width: 480,
            height: 360,
          },
        },
        channelTitle: "Tesla",
        liveBroadcastContent: "none",
        publishTime: "2017-12-14T18:17:31Z",
      },
    },
    {
      kind: "youtube#searchResult",
      etag: "DbZbXHGIkrwjqtfNQR2LhHDvuIc",
      id: {
        kind: "youtube#video",
        videoId: "Ft1waA3p2_w",
      },
      snippet: {
        publishedAt: "2019-10-15T16:00:41Z",
        channelId: "UC1KmNKYC1l0stjctkGswl6g",
        title: "Nikola Tesla - Limitless Energy &amp; the Pyramids of Egypt",
        description:
          "Stream 2 full seasons of Ancient Civilizations for free at http://bit.ly/Ancient_Civilizations only on Gaia. DISCLOSURE: This post may contain affiliate links, ...",
        thumbnails: {
          default: {
            url: "https://i.ytimg.com/vi/Ft1waA3p2_w/default.jpg",
            width: 120,
            height: 90,
          },
          medium: {
            url: "https://i.ytimg.com/vi/Ft1waA3p2_w/mqdefault.jpg",
            width: 320,
            height: 180,
          },
          high: {
            url: "https://i.ytimg.com/vi/Ft1waA3p2_w/hqdefault.jpg",
            width: 480,
            height: 360,
          },
        },
        channelTitle: "After Skool",
        liveBroadcastContent: "none",
        publishTime: "2019-10-15T16:00:41Z",
      },
    },
    {
      kind: "youtube#searchResult",
      etag: "xivb-ytXOZ7uKaDuf9uUKgPdaiA",
      id: {
        kind: "youtube#video",
        videoId: "AGtDszVPgKo",
      },
      snippet: {
        publishedAt: "2018-12-22T14:01:56Z",
        channelId: "UCKHhA5hN2UohhFDfNXB_cvQ",
        title: "O Tesla é tudo isso mesmo? Nós testamos!",
        description:
          "CONHEÇA A NOSSA LOJA: https://lojadomanual.com.br Vocês votaram e a gente foi dar uma volta em um Tesla Model S e mostrar se esse carro elétrico é tudo ...",
        thumbnails: {
          default: {
            url: "https://i.ytimg.com/vi/AGtDszVPgKo/default.jpg",
            width: 120,
            height: 90,
          },
          medium: {
            url: "https://i.ytimg.com/vi/AGtDszVPgKo/mqdefault.jpg",
            width: 320,
            height: 180,
          },
          high: {
            url: "https://i.ytimg.com/vi/AGtDszVPgKo/hqdefault.jpg",
            width: 480,
            height: 360,
          },
        },
        channelTitle: "Manual do Mundo",
        liveBroadcastContent: "none",
        publishTime: "2018-12-22T14:01:56Z",
      },
    },
    {
      kind: "youtube#searchResult",
      etag: "-sWS0gyEDtKSKtriSnk014jGTdo",
      id: {
        kind: "youtube#video",
        videoId: "Beucz-UyREs",
      },
      snippet: {
        publishedAt: "2017-11-09T15:01:06Z",
        channelId: "UC7Lu-ONZhIM_lvKnZn0F19Q",
        title: "TÜRKİYE&#39;DEKİ İLK YENİ “TESLA&#39;&#39; VİDEOSU - Elon MUSK",
        description:
          "Arkadaşlar Merhaba! Bugün yine manyak bir video ile karşınızdayız! Bu videomuzda; sizler için Türkiye'de daha önce hiç videosu çekilmeyen ''TESLA'' aracını ...",
        thumbnails: {
          default: {
            url: "https://i.ytimg.com/vi/Beucz-UyREs/default.jpg",
            width: 120,
            height: 90,
          },
          medium: {
            url: "https://i.ytimg.com/vi/Beucz-UyREs/mqdefault.jpg",
            width: 320,
            height: 180,
          },
          high: {
            url: "https://i.ytimg.com/vi/Beucz-UyREs/hqdefault.jpg",
            width: 480,
            height: 360,
          },
        },
        channelTitle: "Kafalar",
        liveBroadcastContent: "none",
        publishTime: "2017-11-09T15:01:06Z",
      },
    },
    {
      kind: "youtube#searchResult",
      etag: "yqUm-zylsfLVqDelFL-G2jV3MRo",
      id: {
        kind: "youtube#video",
        videoId: "xAXDcTH6hPw",
      },
      snippet: {
        publishedAt: "2020-03-24T15:57:06Z",
        channelId: "UCsqjHFMB_JYTaEnf_vmTNqg",
        title: "The Tesla Model Y Is the Tesla Everyone Is Waiting For",
        description:
          "The Tesla Model Y is the newest Tesla -- and it's eagerly anticipated. Today I'm reviewing the Model Y, and I'm going to show you the quirks and features of the ...",
        thumbnails: {
          default: {
            url: "https://i.ytimg.com/vi/xAXDcTH6hPw/default.jpg",
            width: 120,
            height: 90,
          },
          medium: {
            url: "https://i.ytimg.com/vi/xAXDcTH6hPw/mqdefault.jpg",
            width: 320,
            height: 180,
          },
          high: {
            url: "https://i.ytimg.com/vi/xAXDcTH6hPw/hqdefault.jpg",
            width: 480,
            height: 360,
          },
        },
        channelTitle: "Doug DeMuro",
        liveBroadcastContent: "none",
        publishTime: "2020-03-24T15:57:06Z",
      },
    },
  ],
};

export default data;
